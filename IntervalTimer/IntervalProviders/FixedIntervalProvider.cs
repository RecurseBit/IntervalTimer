﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace IntervalTimer.IntervalProviders
{
    public class FixedIntervalProvider : IIntervalProvider
    {
        private TimeSpan _workSpan;
        private TimeSpan _restSpan;
        private TimeSpan _restLongSpan;

        public void Init() {
            string workMinutesStr = ConfigurationManager.AppSettings["WORK_MINUTES"] ?? "25";
            string restMinutesStr = ConfigurationManager.AppSettings["REST_MINUTES"] ?? "5";
            string restMinutesLongStr = ConfigurationManager.AppSettings["REST_MINUTES_LONG"] ?? "15";

            int workMinutes = 20, restMinutes = 5, restMinutesLong = 15;
            int.TryParse(workMinutesStr, out workMinutes);
            int.TryParse(restMinutesStr, out restMinutes);
            int.TryParse(restMinutesLongStr, out restMinutesLong);

            if (1 == 1) {
                _workSpan = new TimeSpan(0, workMinutes, 0);
                _restSpan = new TimeSpan(0, restMinutes, 0);
                _restLongSpan = new TimeSpan(0, restMinutesLong, 0);
            }
            else {
                _workSpan = new TimeSpan(0, 0, 5);
                _restSpan = new TimeSpan(0, 0, 3);
                _restLongSpan = new TimeSpan(0, 0, 8);
            }
        }

        public TimeSpan GetBreakInterval(int iterationNumber) {
            if (iterationNumber % 4 == 0)
                return _restLongSpan;
            return _restSpan;
        }

        public TimeSpan GetWorkInterval(int iterationNumber) {
            return _workSpan;
        }
    }
}
