﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntervalTimer.IntervalProviders
{
    public class SetIntervalProvider : IIntervalProvider
    {
        public void Init() {
            _workIntervals = new List<int>();
            _breakIntervals = new List<int>();
            // WORK_MINUTES_SET
            // REST_MINUTES_SET
            string workMinutesArrayStr = ConfigurationManager.AppSettings["WORK_MINUTES_SET"] ?? "25";
            string restMinutesArrayStr = ConfigurationManager.AppSettings["REST_MINUTES_SET"] ?? "5";

            var splits = workMinutesArrayStr.Split(new string[] {","}, StringSplitOptions.RemoveEmptyEntries);
            foreach (var s in splits) {
                _workIntervals.Add(int.Parse(s));
            }
            splits = restMinutesArrayStr.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var s in splits) {
                _breakIntervals.Add(int.Parse(s));
            }
        }

        private List<int> _workIntervals;
        private List<int> _breakIntervals;

        public TimeSpan GetWorkInterval(int iterationNumber) {
            if (iterationNumber > _workIntervals.Count + 1) {
                var last = _workIntervals.Last();
                return new TimeSpan(0, last, 0);
            }

            var i = _workIntervals[iterationNumber];
            return new TimeSpan(0, i, 0);
        }

        public TimeSpan GetBreakInterval(int iterationNumber) {
            if (iterationNumber > _breakIntervals.Count + 1) {
                var last = _breakIntervals.Last();
                return new TimeSpan(0, last, 0);
            }

            var i = _breakIntervals[iterationNumber];
            return new TimeSpan(0, i, 0);
        }
    }
}
