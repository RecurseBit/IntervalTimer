﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntervalTimer.IntervalProviders
{
    public interface IIntervalProvider
    {
        void Init();
        TimeSpan GetWorkInterval(int iterationNumber);
        TimeSpan GetBreakInterval(int iterationNumber);
    }
}
